terraform {
    backend "s3" {
        bucket = "my-terraform-cicd-backend"
        key = "statefile/terraform.tfstate"
        region = "us-east-1"
        dynamodb_table = "terraform_state_locking"
      
    }
}