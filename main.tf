
#root

module "vpc"{
    source = "./vpc"
}

module "web" {
    source = "./web"
    my_public_subnet = module.vpc.my_public_subnet
    my_webserver_sg = module.vpc.my_sg
}   