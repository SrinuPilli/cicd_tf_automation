# 1 vpc , 1 subnet and 1 security group

resource "aws_vpc" "terraform_cicd" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support = true

  tags = {
    Name = "terraform_cicd"
    Environment = "Demo"
  }
}

resource "aws_subnet" "pb_subnet" {
    vpc_id = aws_vpc.terraform_cicd.id
    cidr_block = "10.0.1.0/24"
    map_public_ip_on_launch = true
    availability_zone = "us-east-1a"

    tags = {
        Name       = "pb_subnet"
        Environment = "Demo"

    }
  
}

resource "aws_security_group" "web_sg" {
    vpc_id = aws_vpc.terraform_cicd.id
    name   = "my_web_sg"
    description = "Public access to my webserver"

    ingress  {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

}