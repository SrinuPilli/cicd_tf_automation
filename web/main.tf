# 1 ec2 Instance 

resource "aws_instance" "web_server" {
    ami = "ami-00beae93a2d981137"
    instance_type = "t2.micro"
    subnet_id = var.my_public_subnet
    security_groups = [var.my_webserver_sg]

    tags = {
      Name = "My_WebServer"
    }

}